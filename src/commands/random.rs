use poise::serenity_prelude as serenity;
use rand::RngCore;

use crate::commands::{Context, Error};

/// Throws a coin
///
/// Command that does a coinflip.
/// Should only result in heads/tails.
#[poise::command(slash_command, prefix_command, category = "random")]
pub async fn coinflip(ctx: Context<'_>) -> Result<(), Error> {
    let result = if rand::random() {
        "Heads"
    } else {
        "Tails"
    };

    let response = format!("Coin flipped at: {}", result);

    ctx.say(response).await?;
    
    Ok(())
}
