pub mod age;
pub mod random;

pub use age::*;
pub use random::*;

pub(crate) struct Data {}
pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Context<'a> = poise::Context<'a, Data, Error>;
