use std::fmt::Display;

pub fn info(from: impl Display, msg: impl Display) {
    tracing::info!("[{0}] {1}", from, msg)
}

pub fn error(from: impl Display, msg: impl Display) {
    tracing::error!("[{0}] {1}", from, msg)
}

pub fn warn(from: impl Display, msg: impl Display) {
    tracing::warn!("[{0}] {1}", from, msg)
}
