mod commands;
mod config;
mod logger;

use dotenvy_macro::dotenv;
use poise::serenity_prelude as serenity;

#[tokio::main]
async fn main() {
    config::run_all_config();

    let token = dotenv!("DISCORD_TOKEN");
    let intents =
        serenity::GatewayIntents::non_privileged() | serenity::GatewayIntents::MESSAGE_CONTENT;

    let options = config::get_poise_options();

    let framework = poise::Framework::builder()
        .setup(|ctx, ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;

                let msg = format!("Bot {} ready to be used.", ready.user.name);
                logger::info("Ready", msg);

                Ok(commands::Data {})
            })
        })
        .options(options)
        .build();

    let client = serenity::ClientBuilder::new(token, intents)
        .framework(framework)
        .await;

    client.unwrap().start().await.unwrap();
}
