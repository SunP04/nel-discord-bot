use time::macros::format_description;
use time::UtcOffset;
use tracing_subscriber::fmt::time::OffsetTime;

use crate::commands;

pub fn logger_config() {
    let offset = UtcOffset::current_local_offset().expect("should get local offset!");
    let timer = OffsetTime::new(
        offset,
        format_description!("[day]/[month]/[year] [hour]:[minute]"),
    );

    let format = tracing_subscriber::fmt::format()
        .with_ansi(true)
        .with_level(true)
        .with_target(false)
        .with_timer(timer)
        .compact();

    tracing_subscriber::fmt().event_format(format).init();
}

pub fn run_all_config() {
    logger_config();
}

pub fn get_poise_options() -> poise::FrameworkOptions<commands::Data, commands::Error> {
    poise::FrameworkOptions {
        prefix_options: poise::PrefixFrameworkOptions {
            prefix: Some(String::from("el!")),
            ignore_bots: true,
            execute_self_messages: false,
            ..Default::default()
        },
        commands: vec![commands::age(), commands::coinflip()],
        command_check: Some(|ctx| {
            Box::pin(async move {
                let bot = ctx.framework().bot_id;
                let user = ctx.author().id;

                Ok(bot != user)
            })
        }),
        pre_command: |ctx| {
            Box::pin(async move {
                // tracing::info!("Called command: {}", ctx.command().qualified_name);
                let cmd = ctx.command();
                let location = match ctx.guild() {
                    Some(s) => format!("server \"{}\"", s.name),
                    None => format!("\"{}\" private messages", ctx.author().name),
                };
                let msg = format!("Called command \"{}\" in {}", cmd.qualified_name, location);
                crate::logger::info("PreCommand", msg)
            })
        },
        ..Default::default()
    }
}
